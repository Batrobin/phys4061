import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.mplot3d import Axes3D

crystal_type = input("Please input crystal type. (e.g. sc, bcc, fcc or diamond)\n")
repetition = input("Please input the number of repetition for each direction. (e.g. 3 3 3)\n")
repetition = np.array(list(map(int, repetition.split(" "))))

unit_cell_position_dict = {\
 "sc":np.array([[0,0,0]]),\
 "bcc":np.array([[0,0,0],[.5,.5,.5]]),\
 "fcc":np.array([[0,0,0],[0,.5,.5],[.5,0,.5],[.5,.5,0]]),\
 "diamond":np.array([[0,0,0],[0,.5,.5],[.5,0,.5],[.5,.5,0],[.75,.75,.75],[.75,.25,.25],[.25,.75,.25],[.25,.25,.75]])}

assert crystal_type in unit_cell_position_dict, "%s is not a valid crystal type.\n" % crystal_type
assert np.all(repetition>0), "The amount of repetitions must be larger than zero in all axes."

unit_cell_position = unit_cell_position_dict[crystal_type]

X, Y, Z = np.meshgrid(*map(np.arange, repetition))
X = X[:,:,:, np.newaxis] + unit_cell_position[:,0]
Y = Y[:,:,:, np.newaxis] + unit_cell_position[:,1]
Z = Z[:,:,:, np.newaxis] + unit_cell_position[:,2]
X = np.flatten(X)
Y = np.flatten(Y)
Z = np.flatten(Z)

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter(X, Y, Z)
plt.show()
